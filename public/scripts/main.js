 // Test jQuery work
/*$(document).ready(function(){
 alert(jQuery.fn.jquery);
 });*/

function sendData() {
	var valuecpu = 0;
	let xhr = new XMLHttpRequest();
	var select = document.getElementById('select');
	var queryString = '';
	// I catch ex. "=" != "==". Just be more paranoic and wtire "1 != select.value"!
	if(select.value == "1"){
		queryString = 'type=CPU&val='+document.getElementById('rate').value+'&valuetext=NULL';
	} else if (select.value == "2"){
		queryString = 'type=RAM&val='+document.getElementById('rate').value+'&valuetext=NULL';		
	} else if (select.value == "3"){
		//var data = $.get("./get_data/CPU", "json");
		var xhr1 = new XMLHttpRequest();
		xhr1.open('GET', '/get_data/CPU', false);
		xhr1.send();
		var array = JSON.parse(xhr1.responseText);
		queryString = 'type=CPU&val='+array[array.length - 1]+'&valuetext='+document.getElementById('rate').value;
	}
	xhr.open('GET', '/insert_data?'+queryString, true);		
	xhr.send();
	xhr.onload = function() {
		if (xhr.status != 200) { // HTTP ошибка?
			// обработаем ошибку
			alert( 'Ошибка: ' + xhr.status);
			return;
		}
		}
}

timeUpdate = function () {
	var timestamp = document.getElementById('time');
		t = new Date();
	hours = t.getHours();
	min = t.getMinutes() + '';
	if (min.length == 1) min = '0' + min;
	timestamp.innerHTML = t.getDate() + '.' + t.getMonth() + '.' + t.getFullYear() + '<br />' + hours + ':' + min;
};
initChart = function () {
	var MONTHS = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
	var config = {
		type: 'line',
		data: {
			labels: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'],
			datasets: [{
					label: 'CPU (%)',
					backgroundColor: 'rgb(54, 162, 235)',
					borderColor: 'rgb(54, 162, 235)',
					data: [],
					fill: false,
				},
				{
					label: 'RAM (%)',
					backgroundColor: 'rgb(54, 162, 235)',
					borderColor: 'rgb(255, 0, 0)',
					data: [],
					fill: false,
			}
			]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'Загрузка CPU'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Время'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: '%'
					}
				}]
			}
		}
	};
	config.data.datasets[0].data = [];
	config.data.datasets[1].data = [];

	window.myConfig = config;
	var ctx = document.getElementById('cpu_chart').getContext('2d');
	window.myLine = new Chart(ctx, config);
	updateChart();
}
updateChart = function () {
	$.get("./get_data/CPU", function (monData) {
		window.myLine.data.datasets[0].data = monData;
		window.myLine.update();
		if(monData[monData.length - 1] > 90){	
			cpustatus = document.getElementById('cpustatus');
			cpustatus.innerHTML = monData[monData.length - 1];
			cpustatus.style.backgroundColor = 'maroon';
			document.title = "Критическое состояние!";
		} else {
			cpustatus = document.getElementById('cpustatus');
			cpustatus.innerHTML = monData[monData.length - 1];	
			cpustatus.style.backgroundColor = 'green';
			document.title = "CPU, RAM  monito";
		}
	}, "json");
	$.get("./get_data/RAM", function (monData) {
		// console.log(monData);
		window.myLine.data.datasets[1].data = monData;
		window.myLine.update();
	}, "json");
	
	$.get("./get_data_log", function (mondata) {
		// console.log(mondata);
		var log = document.getElementById('log');
		log.innerHTML = mondata.join('\n');			
	}, "json");
	window.myLine.update();

}
window.onload = function () {
	timeUpdate();
	initChart();            
};
setInterval(timeUpdate, 3000);
setInterval(updateChart, 3000);

function response(type, val) {
	let xhr = new XMLHttpRequest();
	queryString = 'type='+type+'&val='+val+'&valuetext=NULL';
	xhr.open('GET', '/insert_data?'+queryString, true);		
	xhr.send();
	xhr.onload = function() {
		if (xhr.status != 200) { // HTTP ошибка?
			// обработаем ошибку
			alert( 'Ошибка: ' + xhr.status);
			return;
		}
	}
}
